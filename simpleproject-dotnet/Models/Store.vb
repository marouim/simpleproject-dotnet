﻿Public Class Store

    Private _name As String
    Private _address As String

    Public Property name() As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    Public Property address() As String
        Get
            Return _address
        End Get
        Set(value As String)
            _address = value
        End Set
    End Property

End Class
