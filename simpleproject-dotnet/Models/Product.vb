﻿Public Class Product

    Private _name As String
    Private _description As String
    Private _price As Single

    Public Property name() As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(value As String)
            _description = value
        End Set
    End Property

    Public Property price() As Single
        Get
            Return _price
        End Get
        Set(value As Single)
            _price = value
        End Set
    End Property
End Class
