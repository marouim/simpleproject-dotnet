﻿Imports System.Net
Imports System.Web.Http

Public Class ProductsController
    Inherits ApiController

    ' GET api/products
    Public Function GetProducts() As IEnumerable(Of Product)

        Dim p1 As Product = New Product()
        p1.name = "IBM 386DX 2GB RAM"
        p1.description = "Special DEMO: Ecran XVGA + Imprimante 9 aiguilles"
        p1.price = 1455.99

        Dim p2 As Product = New Product()
        p2.name = "ACER Pentium II"
        p2.description = "Le plus puissant ! Ecran XVGA + Clavier + Souris"
        p2.price = 2455.99

        Dim p3 As Product = New Product()
        p3.name = "HP Pentium II SECC 66Mhz!"
        p3.description = "Ecran XVGA + Carte video ATI 4MO + Clavier + Souris"
        p3.price = 1990.34

        Return {p1, p2, p3}
    End Function


End Class
