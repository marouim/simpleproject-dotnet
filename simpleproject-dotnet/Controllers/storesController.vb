﻿Imports System.Net
Imports System.Web.Http

Public Class StoresController
    Inherits ApiController

    ' GET api/stores
    Public Function GetStores() As IEnumerable(Of Store)

        Dim s1 As Store = New Store()
        s1.name = "Radioshack"
        s1.address = "Galeries de la Capitale"

        Dim s2 As Store = New Store()
        s2.name = "Electronique Plus"
        s2.address = "Place Laurier"

        Dim s3 As Store = New Store()
        s3.name = "Centre de l'ordinateur"
        s3.address = "boul. Hamel"

        Return {s1, s2, s3}
    End Function


End Class
