﻿Imports System.Net
Imports System.Web.Http

Public Class ClientsController
    Inherits ApiController

    ' GET api/customers
    Public Function GetClients() As IEnumerable(Of Customer)

        Dim c1 As Customer = New Customer()
        c1.nom = "c1"
        c1.email = "c1@simpleproject.com"

        Dim c2 As Customer = New Customer()
        c2.Nom = "c2"
        c2.email = "c2@simpleproject.com"

        Return {c1, c2}
    End Function

    Public Function GetClient(ByVal c As String) As Customer
        Dim c1 As Customer = New Customer()
        c1.nom = "c1"
        c1.email = "c1@simpleproject.com"

        Return c1

    End Function
End Class
